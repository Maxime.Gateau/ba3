#!/bin/bash

file="main.pdf"
course="analyse-complexe"

if [ -f "$course/$file" ] ; then
    rm "$course/$file"
fi

# Compile document
pdflatex -interaction=nonstopmode -output-directory $course/out $course/main.tex

# Compile nomenclature
# makeindex main.nlo -s nomencl.ist -o main.nls

# Compile index
# makeindex main

# Compile bibliography
if [ -f "$course/out/$file" ] ; then
    # Compile bibliography
    biber -output-directory $course/out main
    # Compile document, again
    pdflatex -interaction=nonstopmode -output-directory $course/out $course/main.tex
fi

# Compile glossary
# makeglossaries main

# Once done, move PDF to working directory
mv $course/out/main.pdf $course/main.pdf