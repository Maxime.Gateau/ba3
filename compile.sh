#!/bin/bash

echo Saving Algèbre II...
bash algebre.sh

echo Saving Analyse II - Complexe...
bash complexe.sh

echo Saving Analyse II - Réelle...
bash reelle.sh

echo Saving Analyse Numérique...
bash numerique.sh

echo Saving Topologie Générale...
bash topologie.sh

git add *
read -p "Enter a commit message: " commit_msg
git commit -m "$commit_msg"
git push